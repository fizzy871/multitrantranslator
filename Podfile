source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '7.0'

####################################################################################################
#   Standard SDKs - This section should be included in every new project.
####################################################################################################
#   Push Notifications
#pod 'Pushwoosh'                 # Push Notifications

#   Analytics
#pod 'FlurrySDK'                 # Analytics
#pod 'GoogleAnalytics-iOS-SDK'   # Analytics with Real-Time stats from Google.
#pod 'Facebook-iOS-SDK'          # Facebook Mobile App Install Ad Tracking, Analytic, and Facebook Login (if needed)
#pod 'GoogleAnalytics', '~> 3.13.0' # Google Analytics - measure your app performance
#pod 'Google/Analytics'
#pod 'FBSDKShareKit', '~> 4.4.0'

####################################################################################################
#   Stardard Ad Networks - This section should be included in every new project.
####################################################################################################

#   Video Ads
#   Vungle does not have an up to date CocoaPod so their SDK will need to be added manually.
#pod 'AdColony'                          # Video Ad Network

#   Interstitial Ads
#   Amazon Mobile Ads does not have a CocoaPod so their SDK will need to be added manually.
#   AppLovin does not have a CocoaPod so their SDK will need to be added manually.
#pod 'ChartboostSDK'                     # Interstial Ad Network used for cross-promotion and direct deals
#pod 'PlayHavenSDK'                      # Interstial Ad Network used for cross-promotion and direct deals

#   Banner Ads
#pod 'Google-Mobile-Ads-SDK'             # AdMob Banner Ads and Interstitials
#pod 'CJPAdController'                   # CJPAdController is a singleton class allowing easy implementation of iAds and Google AdMob ads in your iOS app. It supports all devices and orientations, and works on iOS 5.0+
#pod 'RevMob', '~> 7.5.5'                # Official RevMob iOS SDK.
#pod 'SupersonicAds'                      # SupersonicAds SDK providing mobile Ads
#pod 'SupersonicSDK', '~> 6.2.1'          # The Leading Mobile Advertising Technology Platform

#pod 'Tapdaq'                             # Trade installs with other developers and cross promote your own app network


####################################################################################################
#   Project Specific Pods.  This section can be diferent for each project.
####################################################################################################

#pod 'Async', :git => 'https://github.com/duemunk/Async.git'    #Syntactic sugar in Swift for asynchronous dispatches in Grand Central Dispatch (GCD)
pod 'BlocksKit', '~> 2.2'  #BlocksKit hopes to facilitate this kind of programming by removing some of the annoying - and, in some cases, impeding - limits on coding with blocks.
#pod 'Haneke', '~> 1.0' #A lightweight zero-config image cache for iOS written in Objective-C. A Swift version is also available.

######################
# NETWORKING & PARSING
######################
pod 'AFNetworking', '~> 2.0'            # AFNetworking is a delightful networking library for iOS and Mac OS X

#pod 'MWFeedParser'                      # MWFeedParser is an Objective-C framework for downloading and parsing RSS (1.* and 2.*) and Atom web feeds. It is a very simple and clean implementation that reads the following information from a web feed:
#pod 'SBJson'                            # SBJson’s number one feature is chunk-based operation. Feed the parser one or more chunks of UTF8-encoded data and it will call a block you provide with each root-level document or array. Or, optionally, for each top-level entry in each root-level array.


###############
# BACKEND STACK
###############
#pod 'Parse', '~> 1.4.1'                 # Parse is a complete technology stack to power your app's backend.
#pod 'ParseUI', '~> 1.1'    #ParseUI is a collection of a handy user interface components to be used with Parse iOS SDK
#pod 'ParseFacebookUtils', '~> 1.4.1'    # Parse is a complete technology stack to power your app's backend.
#pod 'ParseFacebookUtilsV4', '~> 1.7.5.3'



###############
# BUG TRACKING
###############
#pod 'Instabug'
#pod 'Fabric'
#pod 'Crashlytics'


#####################
# ONBOARDING TUTORIAL
#####################
#pod 'EAIntroView'
#pod 'TTTAttributedLabel'


##########################
# MENUS AND USER INTERFACE
##########################
#    pod 'XLPagerTabStrip', '~> 1.1'    #XLPagerTabStrip is a Container View Controller that allows us to switch easily among a collection of view controllers.
#pod 'SCLAlertView-Objective-C' #Animated Alert View written in Swift but ported to Objective-C, which can be used as a UIAlertView or UIAlertController replacement.

#pod 'MPSkewed'                          # Skewed collection view cells with Parallax inspired by http://capptivate.co/2014/01/18/timbre-2/.
#pod 'JSQMessagesViewController'         # An elegant messages UI library for iOS.
#pod 'CHTumblrMenu'                      # Bubble up style menu used to show menu options when tapping a user in the chat room.
#pod 'MGFashionMenuView'                 # MGFashionMenuView is a view with an awesome animation when it is shown/hidden. It is useful to present a menu, a notification or an action button.  This is the bouncy drop down alert that users see when they subscribe to a chat room.

# HEADS UP DISPLAYS - Choose 1
#pod 'MBProgressHUD'
#pod 'ProgressHUD', '~> 1.3'                       # ProgressHUD is a lightweight and easy-to-use HUD for iOS 8. (Objective-C)

# SIDE DRAW MENUS - Choose 1
#pod 'RESideMenu'
#pod 'MSDynamicsDrawerViewController'    # A container view controller that manages the presentation of a single "pane" view controller overlaid over one or two "drawer" view controllers. The drawer view controllers are hidden by default, but can be exposed by a user-initiated swipe in the direction that that drawer view controller is hidden in. It uses UIKit Dynamics for all animation—there's not a single call to animateWithDuration:animations: in the project.
#pod 'ABCustomUINavigationController'    #   For Flip Squares UI Navigation
#pod 'TTOpenInAppActivity'               # A UIActivity subclass that provides an "Open In ..." action to a UIActivityViewController. TTOpenInAppActivity uses an UIDocumentInteractionController to present all Apps than can handle the document specified with by the activity items.
#pod 'HexColors'


###############
# PHOTO VIEWING
###############
#pod 'MHVideoPhotoGallery', '~> 1.6'
#pod 'MWPhotoBrowser'                    # A simple iOS photo browser with optional grid view, captions and selections.
#pod 'SDWebImage', '~> 3.5.2'            # This library provides a category for UIImageView with support for remote images coming from the web.


################
# SOCIAL SHARING
################
#pod 'AAShareBubbles'                #   Animated Social share buttons control for iOS
#pod 'LINEActivity'                  #   This adds LINE as a choice in the Activity Menu when you hit share.
#pod 'MMMWhatsAppActivity'           #   This adds Whatsapp as a choice in the Activity Menu when you hit share.
#pod 'DMActivityInstagram'           #   This adds Instagram as a choice in the Activity Menu when you hit share.

##############
# APP SETTINGS
##############
# For making NSUserDefaults easier.
#pod 'PAPreferences'
#pod 'InAppSettingsKit', '~> 2.1'        # This iPhone framework allows settings to be in-app in addition to being in the Settings app.