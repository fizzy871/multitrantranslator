//
//  ViewController.m
//  translater
//
//  Created by Alexey Galaev on 05/12/14.
//  Copyright (c) 2014 me. All rights reserved.
//

#import "MainViewController.h"
#import "AFNetworking.h"
#import "HTMLParser.h"
#import "HTMLNode.h"
#import "BlocksKit+UIKit.h"
#import "dataManager.h"

@interface MainViewController ()<UITextFieldDelegate, UIWebViewDelegate> {
    NSMutableData *receiveData;
}

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, weak) IBOutlet UIButton *langButton;
@property (strong, nonatomic) IBOutlet UITextField *tranWorldTextField;

@property (nonatomic, strong) LanguageObject *currentLanguage;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.webView loadHTMLString:@"" baseURL:nil];
    
    self.currentLanguage = [dataManager currentLanguageObject];
    if (!self.currentLanguage) {
        self.currentLanguage = [LanguageObject languages][0];
    }
}

- (IBAction)editBegin:(id)sender {
    self.tranWorldTextField.text = @"";
}

- (IBAction)editingend:(id)sender {
    
}

- (IBAction)translate:(id)sender{
    [self.activityIndicatorView startAnimating];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.multitran.ru/c/m.exe?CL=1&s=%@&l1=%ld",
                           self.tranWorldTextField.text,
                           (long)self.currentLanguage.siteCode.integerValue];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSMutableURLRequest *rq = [NSMutableURLRequest requestWithURL:url];
    [rq setHTTPMethod:@"GET"];
    [rq setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    [rq setValue:@"gzip" forHTTPHeaderField:@"Content-Encoding"];
    NSURLConnection *feedConnection = [[NSURLConnection alloc] initWithRequest:rq delegate:self];
    [feedConnection start];

    [self.tranWorldTextField resignFirstResponder];
}

//MANAGING CONNECTION
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if ([error code] == kCFURLErrorNotConnectedToInternet) {
        NSLog(@"ошибка подключения к сети %@",error);
    }
    else {
        NSLog(@"ошибка получения данных %@",error);
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    
    if([httpResponse statusCode]<300)
    {
        // все ок. Начинаем прием данных
        receiveData = [[NSMutableData alloc] initWithCapacity:2000];
    }
    else
    {
        // сервер вернул ошибку
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receiveData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error = nil;
        //Запускаем HTML парсер
        NSString *myString = [[NSString alloc]initWithData:receiveData encoding:11];
        HTMLParser *parser = [[HTMLParser alloc] initWithString:myString error:&error];
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        HTMLNode *bodyNode = [parser body];
        
        NSArray *spanNodes = [bodyNode findChildTags:@"a"];
        
        HTMLNode *wordsTableNode;
        
        for (HTMLNode *spanNode in spanNodes) {
            
            if ([[spanNode getAttributeNamed:@"href"] containsString:@"s1="]) {
                
                HTMLNode *currentNode = spanNode;
                
                while (![currentNode.tagName isEqualToString:@"table"] && currentNode != nil) {
                    currentNode = currentNode.parent;
                }
                
                wordsTableNode = currentNode;
                
                break;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicatorView stopAnimating];
        });
        
        if (wordsTableNode) {
            NSString *htmlString = [NSString stringWithFormat:@"<html><head><style>a {color:#0f4385;text-decoration: none;}</style></head><body>%@</body></html>", wordsTableNode.rawContents];
            [self.webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://multitran.ru"]];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:nil message:@"перевода не найдено" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            });
        }
        
    });
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (IBAction)selectLanguage:(id)sender {
    
    NSArray *buttonObjects = [LanguageObject languages];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
    for (LanguageObject *object in buttonObjects) {
        [actionSheet addButtonWithTitle:object.name];
    }
    [actionSheet bk_setWillDismissBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex != 0) {
            LanguageObject *selectedObject = [LanguageObject languages][buttonIndex-1];
            self.currentLanguage = selectedObject;
            [dataManager setCurrentLanguageObject:selectedObject];
        }
    }];
    [actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}

#pragma mark - textField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (![textField.text isEqualToString:@""]) {
        [self translate:nil];
    }
    return YES;
}

#pragma mark - webviewdelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeOther) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
}

#pragma mark - setters, getters
-(void)setCurrentLanguage:(LanguageObject *)currentLanguage {
    _currentLanguage = currentLanguage;
    [self.langButton setTitle:currentLanguage.languageCode forState:UIControlStateNormal];
    [self textFieldShouldReturn:self.tranWorldTextField];
}

@end