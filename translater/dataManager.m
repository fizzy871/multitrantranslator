//
//  dataManager.m
//  translater
//
//  Created by Алексей Саечников on 06.10.15.
//  Copyright © 2015 me. All rights reserved.
//

#import "dataManager.h"

@implementation LanguageObject

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        self.name = [coder decodeObjectForKey:@"name"];
        self.siteCode = [coder decodeObjectForKey:@"siteCode"];
        self.languageCode = [coder decodeObjectForKey:@"langCode"];
        self.type = [coder decodeIntegerForKey:@"type"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.siteCode forKey:@"siteCode"];
    [coder encodeObject:self.languageCode forKey:@"langCode"];
    [coder encodeInteger:self.type forKey:@"type"];
}

+ (instancetype) language:(NSString*)name siteCode:(NSNumber*)siteCode languageCode:(NSString*)code languageType:(languageType)type;{
    LanguageObject *object = [LanguageObject new];
    object.name = name;
    object.siteCode = siteCode;
    object.languageCode = code;
    object.type = type;
    return object;
}

+ (NSArray*) languages {
    return @[
             [LanguageObject language:@"Английский" siteCode:@(1) languageCode:@"EN" languageType:languageTypeEnglish],
             [LanguageObject language:@"Немецкий" siteCode:@(3) languageCode:@"DE" languageType:languageTypeGerman],
             [LanguageObject language:@"Французский" siteCode:@(4) languageCode:@"FR" languageType:languageTypeFrench],
             [LanguageObject language:@"Испанский" siteCode:@(5) languageCode:@"ES" languageType:languageTypeSpanish],
             [LanguageObject language:@"Итальянский" siteCode:@(23) languageCode:@"IT" languageType:languageTypeItalic],
             [LanguageObject language:@"Нидерландский" siteCode:@(24) languageCode:@"NL" languageType:languageTypeDutch],
             [LanguageObject language:@"Латышский" siteCode:@(27) languageCode:@"LV" languageType:languageTypeLettish],
             [LanguageObject language:@"Эстонский" siteCode:@(26) languageCode:@"ET" languageType:languageTypeEstonian],
             [LanguageObject language:@"Африкаанс" siteCode:@(31) languageCode:@"AF" languageType:languageTypeAfrikaans],
             [LanguageObject language:@"Эсперанто" siteCode:@(34) languageCode:@"EO" languageType:languageTypeEsperanto],
             [LanguageObject language:@"Калмыцкий" siteCode:@(35) languageCode:@"XA" languageType:languageTypeKalmyk]
             ];
}

@end

@implementation dataManager
+ (LanguageObject*) currentLanguageObject {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *objectData = [defaults objectForKey:@"currLang"];
    if (!objectData) {
        return nil;
    }
    else {
        LanguageObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:objectData];
        return object;
    }
}
+ (void) setCurrentLanguageObject:(LanguageObject*)object {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *objectData = [NSKeyedArchiver archivedDataWithRootObject:object];
    [defaults setObject:objectData forKey:@"currLang"];
    [defaults synchronize];
}
@end
