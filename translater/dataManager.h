//
//  dataManager.h
//  translater
//
//  Created by Алексей Саечников on 06.10.15.
//  Copyright © 2015 me. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, languageType) {
    languageTypeEnglish,
    languageTypeGerman,
    languageTypeFrench,
    languageTypeSpanish,
    languageTypeItalic,
    languageTypeDutch,
    languageTypeLettish,
    languageTypeEstonian,
    languageTypeAfrikaans,
    languageTypeEsperanto,
    languageTypeKalmyk
};

@interface LanguageObject : NSObject<NSCoding>
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *siteCode;
@property (nonatomic, strong) NSString *languageCode;
@property (nonatomic) languageType type;
+ (NSArray*) languages;
@end

@interface dataManager : NSObject
+ (LanguageObject*) currentLanguageObject;
+ (void) setCurrentLanguageObject:(LanguageObject*)object;
@end
